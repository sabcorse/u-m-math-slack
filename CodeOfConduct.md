# U-M Math Slack Code of Conduct Introduction: Welcome to the Community!

The U-M Math Slack space is dedicated to providing a welcoming, informative, and fun environment for anyone interested in math at U-M, regardless of math background, gender identity or expression, sexual orientation, disability, physical appearance or attributes, race, age, or religion. We do not tolerate harassment of U-M Math Slack members in any form, as harassment and code of conduct violations reduce the value of this community as an inclusive environment for those with a shared interest in math. Explicit details regarding expected behaviours are outlined in the rest of this document.

# Joining the U-M Math Slack:

Anyone with an interest in mathematics at U-M is welcome to join the community. You can access the U-M Math Slack sign-up form at https://forms.gle/Sr9qoFvonTc8pG9A7 (a precaution to ensure that one agrees to the code of conduct prior to joining). After submitting the form, you should be directed to a page with a sign-in link for the Slack space. Please be sure to be logged into your U-M account when you access the Slack space, as only accounts with umich.edu email addresses will be granted access. If you encounter any issues during this process, please direct questions to sabcorse@umich.edu and evelynzc@umich.edu.

# U-M Math Slack Code of Conduct:

This code of conduct is a living document that is open to suggestions in the U-M Math Slack's #code-of-conduct channel. 

The U-M Math Slack space is dedicated to providing a welcoming, informative, and fun environment for anyone interested in math at U-M, regardless of math background, gender identity or expression, sexual orientation, disability, physical appearance or attributes, race, age, or religion. We do not tolerate harassment of U-M Math Slack members in any form. Sexual language and imagery are not appropriate for this group. As a community designed to foster education and enthusiasm for math, we do not permit dismissive or belittling behavior.

Harassment and code of conduct violations reduce the value of this community as an inclusive environment for those with a shared interest in math. It is up to the discretion of the group moderators to ban members that violate these rules, regardless of the intent behind a violation.

Note that the U-M Math Slack is an informal community not sponsored by U-M or the U-M Math Department. As such, it is run/moderated by volunteers. Users with the ability to moderate code of conduct violations ("admins") do not regularly check all channels for problems. As U-M Math Slack members, we will do our best to keep the community safe and inviting, but please bring any issues to the attention of our admins at sabcorse@umich.edu and evelynzc@umich.edu.

# Prohibited behaviors:

Please note that the following list may not be exhaustive. The determination of actions that constitute harassment (generally any behavior that threatens or demeans another person or group, or produces an unwelcoming environment) is up to the moderators of the group. 

1.  Publication of any private communication without explicit consent.
2.  Unsolicited messages (private or public) to request donations or other funding-related requests.
3.  Offensive comments regarding gender/gender identity/gender expression, sexual orientation, disability, mental illness, physical appearance or attributes, age, race, religion, or political views.
4.  Unwelcome comments regarding a person's lifestyle choices and practices, including those related to food, health, relationships, drugs, employment, or academic status.
5.  Deliberate misgendering or use of dead or rejected names.
6.  Any sexual imagery or behavior, including unwelcome sexual attention.
7.  Descriptions of simulations of physical contact without consent or after a request to stop.
8.  Threats of violence or incitement of violence towards an individual (including encouragement of self-harm).
9.  Deliberate intimidation.
10.  Stalking or following.
11.  Recording or logging online activity for harassment purposes. 
12.  Sustained disruption of discussion (e.g. non-constructive criticism)
13.  Pattern of inappropriate social contact, such as requesting or assuming inappropriate levels of intimacy with others.
14.  Continued one-on-one communication after requests to cease.
15.  Deliberate outing of any aspect of a person's identity without their consent (except as necessary when reporting inappropriate behavior directly to an admin).
16.  Belittling or dismissing people asking for help learning.
17.  Dismissiveness toward harassment or complaints about harassment.
18.  Jokes that incorporate any type of harassment or derogatory stereotypes.
19.  Quoting or reposting any type of harassment (including messages removed by moderators).

# Permanence and Privacy

Slack only makes the most recent 10k messages directly available to all users. However, admins may export the entire message archive (beyond the most recent 10k) as a JSON file. The JSON file is not easily readable itself, but it may be transformed into a more readable format, so please be aware of this possibility.

Your U-M email address will be visible to other members of the U-M Math Slack. 

Please also be aware that messages from the U-M Math Slack could be captured via screenshot or copy/past by any U-M Math Slack user. To allow appropriate reuse of the U-M Math Slack while respecting the privacy and property of the group's members, you must follow the requirements below for quoting or reusing the U-M Math Slack:

*  *Screenshots:* If you want to share a screenshot or quote from the U-M Math Slack, you must receive explicit permission from any user quoted in the shared conversation. This requirement stands even if you block out or otherwise censor the usernames in the conversation.
*  *Message Archive Export Use (Research, Quoting, Etc.):* You should describe your requested use in the #general channel and wait to get an explicit okay from an admin (Sabrina Corsetti or Evelyn Chen), who will synthesize responses and make sure others have enough time to respond.

# Guidance on Being a Good Community Member:

*  The current settings for the U-M Math Slack group permit all users to create public channels. We welcome anyone with ideas for useful new channels to go ahead and make them. However, be aware that if you create a channel, its purpose and content must be compliant with the code of conduct; admins reserve the right to delete non-compliant channels.
*  Default toward believing and working to understand someone when they report harassment or discomfort. Although a particular action might not seem like harassment to someone not regularly aware of such an action, it is best to attempt to understand why said action might make someone feel undervalued or underappreciated. 
*  If you are uncertain about whether something you want to post constitutes harassment, do not guess. Read through these guidelines again, then if necessary reach out to a moderator (Sabrina Corsetti and Evelyn Chen) via direct message or email (sabcorse@umich.edu and evelynzc@umich.edu) to ask.
*  Consider not just the intent of your behavior; remember that the impact of your actions is just as important as your intent. 

# Conflict Resolution:

1.  Initial Incident:
    
    If you have any concerns about harassment of yourself or others and you feel comfortable speaking with the offender, please inform the offender that they have affected you negatively. Often, the offending behavior is unintentional, and the accidental offending and offended parties can resolve the incident by having this initial discussion.
    
    The U-M Math Slack admins understand that you may prefer not to engage directly with the offender for a variety of reasons. If you do not feel comfortable speaking with the offender for any reason, skip straight to step 2.

2.  Escalation:
    
     If the offender denies their offense or actively harasses you - or if direct engagement is not a good option for you - then you are encouraged to involve group admins (via Slack direct messages or at sabcorse@umich.edu and evelynzc@umich.edu). See further details under "Reporting".


# Reporting

If you notice someone making you or another member of the group feel unsafe or unwelcome, please report it as soon as possible to Sabrina Corsetti and Evelyn Chen at sabcorse@umich.edu and evelynzc@umich.edu respectively. Your information will be kept anonymous and never shared unless you explicitly consent to its sharing. Admins will promptly respond to all reported conflicts; you will not be asked to confront anyone yourself, and your identity will not be disclosed.

# Sanctions:

U-M Math Slack members asked to stop any behavior not compliant with this code of conduct will be expected to comply immediately. If a member engages in harassing behavior, organizers may take any action they deem appropriate. Potential sanctions may include but are not limited to:

*  Warning a harasser to cease their behavior and to expect further sanctions in the event of a repeat offense.
*  Requiring that a harasser avoid interaction with another member of the group.
*  Deletion of a comment, conversation, or channel that violates the code of conduct.
*  Banning a member from the Slack space and probiting them from joining again (either indefinitely or for a specific time period).

# Improving the Code of Conduct:

The current owners/admins of the U-M Math Slack are Sabrina Corsetti and Evelyn Chen. As the group attracts more members, we may seek to add more admins to help moderate discussions if needed. Suggestions for improving the moderation of the group or this code of conduct itself are welcome. Please feel free to share any ideas in the #code-of-conduct channel if you feel comfortable doing so, or share them directly with Sabrina Corsetti and Evelyn Chen (via direct message or email at sabcorse@umich.edu and evelynzc@umich.edu).

# License, Attribution, and Credit:

This code of conduct has been developed based on the adaptable codes of conduct set forth by the DH Slack group (https://github.com/amandavisconti/DHslack/blob/master/CodeOfConduct.md) and the code4lib Slack group (https://github.com/code4lib/code-of-conduct/blob/master/code_of_conduct.md), which are in turn based on the Geek Feminism Wiki's code of conduct (http://geekfeminism.wikia.com/wiki/Community_anti-harassment), created by the Ada Iniative and other volunteers. All three codes of conduct are generously CC Zero (licensed under the Creative Commons Zero license).